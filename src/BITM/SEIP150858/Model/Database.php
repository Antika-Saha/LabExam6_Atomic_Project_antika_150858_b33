<?php
namespace App\Model;

use PDO;
use PDOException;


class Database{
    public $conn;

    public $host="localhost";
    public $dbname="antika_atomic_project_b35";
    public $username="root";
    public $password="";


    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=".$this->dbname, $this->username, $this->password);
            echo "success";

        }
        catch(PDOException $e) {
            echo "unsuccess";
            echo $e->getMessage();
        }
    }
}
